import downloadData from './src/stages/downloadData.ts'
import storeData from './src/stages/storeData.ts'
import { env } from './src/lib/env.ts'

;(async () => {
  if (!env.FILEGATOR_URL) {
    throw 'Missing environment variables, check `.env.sample`'
  }
  console.log('Starting...')
  await downloadData()
  await storeData()
  console.log('Done!')
})()
