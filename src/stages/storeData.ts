import getFiles from 'https://deno.land/x/getfiles@v1.0.0/mod.ts'
import render from '../lib/markdown.ts'
import toArticleUrl from '../lib/toArticleUrl.ts'
import { imgproxyUrl } from '../lib/imgproxy.ts'
import { env } from '../lib/env.ts'

import { emptyDir, ensureFile } from 'https://deno.land/std@0.109.0/fs/mod.ts'

type ArticleCard = {
  headline: string
  drophead: string
  date: string
  url: string
  byline: string[]
  caption: string
  image: string
  topics: string[]
}

export default async function (): Promise<void> {
  await emptyDir('./public')
  await emptyDir('./public/articles')
  await emptyDir('./public/topics')

  const articleFiles = getFiles({ root: './download/newspaper-main/articles/' })

  const articlesByTopic: Record<string, ArticleCard[]> = {}
  const articlesByByline: Record<string, ArticleCard[]> = {}

  const articlesList: ArticleCard[] = []

  await Promise.all(
    articleFiles.map(async (file) => {
      const text = await Deno.readTextFile(file.realPath)
      const rendered = render(text)
      const article: Record<string, string> = {
        ...rendered.meta,
        body: rendered.document,
      }

      if (article?.headline === 'Title' || typeof article?.date === undefined) {
        return
      }

      if (typeof article !== 'object') {
        return
      }

      if (
        !article.image?.startsWith('http') &&
        typeof article.image === 'string'
      ) {
        try {
          const sourcePath =
            file.path
              .replace('download/newspaper-main', '')
              .replace('.md', '/') + article.image

          const imageUrl = `${env['FILEGATOR_URL']}/?r=/download&path=${encodeURIComponent(btoa(sourcePath))}`

          article.image = imgproxyUrl(imageUrl)

        } catch (error) {
          console.error(
            `Error creating image link`, article.image
          )
        }
      }

      const date = new Date(rendered.meta.date)

      const fileName =
        './public/articles' +
        toArticleUrl({
          date: date,
          headline: article.headline,
          suffix: '.json',
        })
      await ensureFile(fileName)

      await Deno.writeTextFile(fileName, JSON.stringify(article))
      console.log(`Article: ${article.headline}`)

      const articleCard: ArticleCard = {
        headline: article.headline || '',
        drophead: article.drophead || '',
        date: article.date?.toString() || '',
        url: toArticleUrl({ date: date, headline: article.headline }) || '',
        byline: article.byline?.split(', ') || '',
        caption: article.caption || '',
        image: article.image || '',
        topics: article.topics?.split(', ') || [],
      }

      articleCard.topics.forEach((topic) => {
        if (!articlesByTopic[topic]) {
          articlesByTopic[topic] = []
        }
        articlesByTopic[topic].push(articleCard)
      })

      articleCard.byline.forEach((person) => {
        if (!articlesByByline[person]) {
          articlesByByline[person] = []
        }
        articlesByByline[person].push(articleCard)
      })

      articlesList.push(articleCard)
    })
  )

  await Deno.writeTextFile(
    './public/articles.json',
    JSON.stringify(articlesList.sort(sortArticles))
  )

  console.log(`All articles: ${articlesList.length} total articles`)

  // TOPICS

  const topicsList = Object.keys(articlesByTopic)

  await Promise.all(
    topicsList.map(async (topic) => {
      const articles = articlesByTopic[topic].sort(sortArticles)
      const fileName = `./public/topic/${topic}.json`

      await ensureFile(fileName)

      await Deno.writeTextFile(fileName, JSON.stringify(articles))

      console.log(`Topic: ${topic}`)
    })
  )

  await Deno.writeTextFile('./public/topics.json', JSON.stringify(topicsList))

  console.log(`All topics: ${topicsList.join(', ')}`)

  // STAFF

  interface staffMember {
    name: string
    url: string
    date: string
    years: string[]
  }

  const staffList: staffMember[] = []

  articlesList.forEach((article) => {
    article.byline.forEach((name) => {
      if (!staffList.map((person) => person.name).includes(name))
        staffList.push({
          name,
          url: `/staff/${name.replace(' ', '-')}`,
          date: article.date,
          years: [],
        })

      const indexOf = staffList.map((person) => person.name).indexOf(name)
      const year = new Date(article.date).getFullYear().toString()

      if (!staffList[indexOf].years.includes(year)) {
        staffList[indexOf].years.push(year)
      }
    })
  })

  await Promise.all(
    staffList.map(async (person) => {
      const articles = articlesByByline[person.name].sort(sortArticles)
      const fileName = `./public/staff/${person.name.replace(' ', '-')}.json`

      await ensureFile(fileName)

      await Deno.writeTextFile(
        fileName,
        JSON.stringify({
          ...person,
          articles,
        })
      )

      console.log(`Staff: ${person.name}`)
    })
  )

  await Deno.writeTextFile('./public/staff.json', JSON.stringify(staffList))

  // SEARCH

  interface SearchResult {
    title: string
    url: string
    date: string
    type: string
  }

  const searchList: SearchResult[] = []

  articlesList.forEach((article) =>
    searchList.push({
      title: article.headline,
      url: article.url,
      date: article.date,
      type: 'article',
    })
  )

  staffList.forEach((person) => {
    searchList.push({
      title: person.name,
      url: person.url,
      date: person.date,
      type: 'staff member',
    })
  })

  await Deno.writeTextFile('./public/search.json', JSON.stringify(searchList))

  // INDEX

  await Deno.writeTextFile(
    './public/index.html',
    `
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Pasadena Chronicle Meta</title>
    </head>

    <body>
      <h1>Pasadena Chronicle Meta</h1>
      <p><i>Data last updated ${new Date().toLocaleString('en-US', {
        timeZone: 'America/Los_Angeles',
      })}</i></p>
      <p>This server hosts all of the data (articles, staff members, comics) for the Pasadena Chronicle. It pulls the data from <a href="https://gitlab.com/pasadena-chronicle/newspaper">the newspaper repository.</a></p>
      <p>Treat this as an API for retrieving newspaper-related data.</p>
      <h2>Example routes</h2>
      <ul>
        <li><a href="./articles.json">/articles.json</a></li>
        <li><a href="./articles/02/03/18/Technology-in-Our-Classrooms:-Chromebooks.json">	/articles/02/03/18/Technology-in-Our-Classrooms:-Chromebooks.json</a></li>
      </ul>
      <h2>Source code</h2>
      <p>This meta server is open source and available on <a href="https://gitlab.com/pasadena-chronicle/meta">GitLab.</a></p>
      </p>
    `
  )
  console.log('Index page: created')
}

function sortArticles(a: ArticleCard, b: ArticleCard) {
  const aDate = new Date(a.date)
  const bDate = new Date(b.date)
  return bDate.getTime() - aDate.getTime()
}
