import MarkdownIt from 'https://esm.sh/markdown-it@12.0.6'
import meta from 'https://esm.sh/markdown-it-meta@0.0.1'
import implicitFigures from 'https://esm.sh/markdown-it-implicit-figures@0.10.0'

const md = new MarkdownIt({
  html: true,
  linkify: true,
  typographer: true
})
md.linkify.set({fuzzyLink: true});
md.use(meta)
md.use(implicitFigures, {
  dataType: false,
  figcaption: true,
  tabindex: false,
  link: false
});

export default function render(raw: string): { document : string, meta: Record<string, string> } {
  const renderedDocument = md.render(raw)
  return {
    document: renderedDocument,
    meta: md.meta
  }
}